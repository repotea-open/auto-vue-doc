import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import echarts from 'echarts'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'



Vue.prototype.$echarts = echarts
Vue.use(ElementUI);
Vue.prototype.$axios = axios
Vue.config.productionTip = false

// import { imgsPreloader } from './config/imgPreloader'
// import imgPreloaderList from './config/imgPreloaderList';
let startApp = async () => {
  // await imgsPreloader(imgPreloaderList);
  //关闭加载弹框
  // document.querySelector('.loading').style.display = 'none';

  axios.get('./config.json').then((res) => {
    // 保存 API 地址
    store.commit('SET_BASE_API', res.data.BASE_URL)
    // Vue.prototype.BASE_URL = res.data.BASE_URL;

    
    new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')

  })
};startApp()
