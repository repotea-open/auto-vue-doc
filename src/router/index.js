import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/home/index.vue')
  },
  {
    path: '/doc',
    meta: {
      title: '白羊',
      // internals: [ // 排除显示
      //   'system',
      // ]
      // path: '@/components' // 默认查询组件
    },
    name: 'Doc2',
    component: () => import('../auto-vue-doc/Doc.vue')
  },
  {
    path: '/log',
    name: 'Log',
    component: () => import('../views/system/Log.vue')
  },
  {
    path: '/technicalPrevention',
    name: 'TechnicalPrevention',
    component: () => import('../views/overview/TechnicalPrevention.vue')
  },
  {
    path: '/fireControl',
    name: 'FireControl',
    component: () => import('../views/overview/FireControl.vue')
  },
  {
    path: '/fire',
    name: 'Fire',
    component: () => import('../views/system/Fire.vue')
  },
  {
    path:'/overview',
    name:'Overview',
    component: () => import('../views/system/Overview.vue')
  },
  {
    path:'/echarts',
    name:'Echars',
    component: () => import('../views/system/Echarts.vue')
  },
  {
    path:'/distribution',
    name:'Distribution',
    component: () => import('../views/system/Distribution.vue')
  },
  {
    path:'/case',
    name:'Case',
    component: () => import('../views/system/Case.vue')
  },
  {
    path:'/safety',
    name:'Safety',
    component: () => import('../views/system/Safety.vue')
  },
  {
    path:'/histogram',
    name:'Histogramt',
    component: () => import('../views/system/Histogram.vue')
  },
  {
    path:'/firechart',
    name:'Firechart',
    component: () => import('../views/system/Firechart.vue')
  },
  {
    path:'/linechart',
    name:'Linechart',
    component: () => import('../components/common/Linechart.vue')
  },
  {
    path:'/wheels',
    name:'Wheels',
    component: () => import('../components/common/Wheels.vue')
  },
  {
    path:'/test',
    name:'Test',
    component: () => import('../views/Test.vue')
  }
]

const router = new VueRouter({
  // mode: 'history',
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
