import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: { 
    themes: {
      light: {
        primary: '#09E0F9', // 科技蓝
        secondary: '#424242',
        accent: '#1976D2',
        error: '#FF5252',
        info: '#CDCDCF',  // 灰
        success: '#00FFAE', // 绿
        warning: '#F4E80B' // 黄
      },
      dark: {
        primary: '#2196F3',
        secondary: '#424242',
        accent: '#FF4081',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00'
      }
    }
    
  }
});
