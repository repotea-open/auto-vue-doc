// 日志 API
import request from '@/utils/request'

/**
 * /客户端设置/获取更新日志
 */
export function getVersionUpdate() {
  return request({
    url: '/client/version_update',
    method: 'GET'
  })
}