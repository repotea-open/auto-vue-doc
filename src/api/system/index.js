// 系统参数 API
import request from '@/utils/request'

/**
 * /客户端设置/获取系统参数列表
 */
export function getSystemParameter() {
  return request({
    url: '/client/systemParameter',
    method: 'GET'
  })
}


/**
 * /全校概览/人员情况
 */
export function getPeopleDetail() {
  return request({
    url: '/overview/school/peopleDetail',
    method: 'GET'
  })
}

/**
 * /全校概览/案件情况
 */
export function getEventStatistics() {
  return request({
    url: '/schoolOverview/eventStatistics',
    method: 'GET'
  })
}

/**
 * 全校概览/安全隐患趋势
 *
 * */
export function getSecurityRisksStatistics(campusId) {
    return request({
        url: '/overview/campus/getSecurityRisksStatistics?campusId='+campusId,
        method: 'GET'
    })
}

/**
 * 全校概览/组织结构
 * */
export function organizationStructure(campusId) {
    return request({
        url: '/overview/campus/organizationStructure?campusId='+campusId,
        method: 'GET'
    })
}

/**
 * 全校概览/智能应用系统
 * */
export function intelligentAppSys() {
    return request({
        url: '/overview/campus/intelligentAppSys',
        method: 'GET'
    })
}



/**
 * 首页功能列表
 * */
export function getApps(clientId) {
    return request({
        url: '/client/app?clientId='+clientId,
        method: 'GET'
    })
}


/**
 * 获取校区
 * */
export function getCampus(clientId) {
    return request({
        url: '/home/campus/'+clientId,
        method: 'GET'
    })
}
