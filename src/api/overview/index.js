// 系统参数 API
import request from '@/utils/request'

/**
 * 技防、消防概览/左边系统
 * params: {
 *  mapId	否	1  地图id 
 *  mapType	是	地图类型
 *  deviceType	是	1  1安防 2消防
 * }
 * */
export function systemDetail(params) {
  return request({
      url: '/overview/fireControlSecurity/systemDetail',
      method: 'GET',
      params: params
  })
}

/**
* 技防、消防概览/右边详情
* */
export function detail() {
  return request({
      url: '/overview/fireControlSecurity/detail',
      method: 'GET'
  })
}
