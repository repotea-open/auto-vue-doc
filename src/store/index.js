import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    BASE_API: '',
    // 图片缓存
    image_cache: [
      require('@/assets/common/bg.png'),
      require('@/assets/common/bg2.png'),
      require('@/assets/common/bg3.png'),
      require('@/assets/common/bbk.png')
    ]
  },
  mutations: {
    SET_BASE_API: (state, val) => {
      state.BASE_API = val
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [createPersistedState()]
})
