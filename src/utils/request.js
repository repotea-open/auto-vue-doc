import axios from 'axios'
import store from '../store'
import {
  Message,
  MessageBox
} from 'element-ui'
import {
  removeAuth
} from '@/utils/auth'

// 每次访问 CAS Server 生成不同的 JSESSIONID  开启后文件上传会有跨域问题
// axios.defaults.withCredentials = true

var showLoginMessageBox = false
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'
axios.defaults.headers['Access-Control-Allow-Origin'] = '*'
// 创建axios实例



// async function getConfig() {
//   let res = await axios.get('./config.json')
//   BASE_API = res.data.BASE_URL
// }
const service = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? process.env.VUE_APP_REQUEST_URL : store.state.BASE_API, // api 的 base_url
  timeout: 15000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(
  config => {
    // const flag = config.headers['Content-Type'] && config.headers['Content-Type'].indexOf('application/json') !== -1
    // if (!flag) {
    //   const mult = config.headers['Content-Type'] && config.headers['Content-Type'].indexOf('multipart/form-data') !== -1
    //   if (!mult) {
    //     config.data = config.data
    //   } else {
    //     config.data = qs.stringify(config.data)
    //   }
    // }
    return config
  },
  error => {
    // Do something with request error
    return Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use(
  response => {
    /**
     * code为非20000是抛错 可结合自己业务进行修改
     */
    const res = response.data
    if (response.status === 200 && response.config.responseType === 'blob') { // 文件类型特殊处理
      return response
    } else if (res) {
      return res
    } else {
      return res
    }
  },
  error => {
    const code = error.response.status
    if (code === 401) {
      if (!showLoginMessageBox) {
        showLoginMessageBox = true
        MessageBox.confirm(
          '你已被登出，请重新登录',
          '确定登出', {
          showCancelButton: false,
          showClose: false,
          confirmButtonText: '重新登录',
          type: 'warning',
          callback: action => {
            showLoginMessageBox = false
            if (action === 'confirm') {
              removeAuth().then(() => {
                location.reload() // 为了重新实例化vue-router对象 避免bug
              }).catch(() => {
                location.reload()
              })
            }
          }
        }
        )
        return false
      }
    } else if (code === 404) {
      Message({
        message: '接口 404 查看是否存在此接口或检查路径',
        type: 'error'
      })
    } else if (error.response.data.message && error.response.data.message.indexOf('友情提示;') > -1) {
      let mes = error.response.data.message.substring(5, error.response.data.message.length)
      MessageBox.alert(`${mes}`, '提示', {
        confirmButtonText: '我知道了',
        type: 'warning',
        dangerouslyUseHTMLString: true,
        center: true
      })
    } else {
      Message({
        message: '网络请求失败，请稍候再试',
        type: 'error'
      })
    }
    return Promise.reject(error)
  }
)

export default service
