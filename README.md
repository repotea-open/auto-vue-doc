
<div align="center">
![logo](static/logo.png)
</div>

客户端 web 项目.

客户端 web 负责呈现页面

[[_TOC_]]

## 前置需求

在你开始上手这个项目之前，请检查是否满足以下条件：

* 你有 NodeJS 开发环境
* 你熟悉 Vue 开发
* 你使用 VsCode 作为开发 IDE（可选）
* 你全局安装了 yarm 命令
```
npm install -g yarn
```

## 项目介绍
- public   # 图标和 vue 入口页的文件
  -  favicon.ico 
  -  index.html
- src
  - api # 所有 api 方法
  - assets  # 静态资源
  - components  # 公共组件
  - plugins  # 第三方组件的配置
  - router  # 路由
  - store  # vuex
  - utils  # 工具包
  - views  # vue 页面 按文件夹存放 除index.vue之外，其他.vue文件统一用 PascalBase 风格 
- static # 公共静态资源
- .env.development # 分别代表一种环境的配置文件 开发环境
- .env.production  # 生产环境
- .env.test   # 测试环境
- vue.config.js   # vue 配置信息

## 安装

### 项目设置
```
yarn install
```

### 本地开发启动
```
yarn serve
```

### 生产编译
```
yarn build
```

### 运行单元测试
```
yarn test:unit
```

### lint 和 fixes 文件
```
yarn lint
```


