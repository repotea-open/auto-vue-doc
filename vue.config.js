const webpack = require('webpack')
module.exports = {
  outputDir: process.env.NODE_ENV === 'development' ? 'dist' : "dist",
  indexPath: "index.html",
  publicPath: process.env.NODE_ENV === 'development' ? './' : "./", 
  assetsDir: process.env.NODE_ENV === 'development' ? 'static' : 'static', 
  lintOnSave: true,
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    },
    open: true,
    // host: "localhost", 
    port: "13000",
    https: false,
    hotOnly: false,
    proxy: {
      "/api": {
        target: 'http://192.168.1.169:9991/', //目标接口域名
        pathRewrite: {
          "^/api": "/"
        },
        changeOrigin: true
      }
    }
  },
  "transpileDependencies": [
    "vuetify"
  ],
  chainWebpack: config =>{
    config.plugin('html')
      .tap(args => {
        args[0].title = 'auto-doc'
        return args;
      })
  },
  configureWebpack: {
    plugins: [
       new webpack.ProvidePlugin({
         $:"jquery",
         jQuery:"jquery",
         "windows.jQuery":"jquery"
       })
     ]
 }
}